package net.wangxj.util.ffmpeg;

import net.wangxj.util.encry.Base64Utill;
import net.wangxj.util.encry.MD5;
import net.wangxj.util.encry.MD5Util;
import net.wangxj.util.encry.PBKDF2SHA1;
import org.junit.Test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * @author huoshan
 * created by 2017年5月19日 下午4:01:25
 * 
 */
public class FFmpegTest {
	
	@Test
	public void testCheckType(){
//		System.out.println(FFmpegUtil.checkContentType("MP4"));
//		System.out.println(FFmpegUtil.checkContentType("avi"));
//		System.out.println(FFmpegUtil.checkContentType("rmvb"));
//		System.out.println(FFmpegUtil.checkContentType("mp3"));

	}

	@Test
	public void testGeneratePreviewVideo() throws IOException {
		FFmpegUtil.generatePreviewVideo("/home/free/文档/a.mp4", "/home/free/文档/preview.mp4", "320:180");
	}

	@Test
	public void testGenerateTsAndM3u8()throws Exception{
//		FFmpegUtil.coverMp4ToTs("/home/free/下载/a.mp4", "/home/free/下载/a.ts");
		FFmpegUtil.convertTsToM3u8("/home/free/下载/a.ts", "/home/free/下载/a.m3u8", "/home/free/下载/ts/abc%03d.ts", 10);
	}

	@Test
	public void testGetTime(){
		System.out.println(FFmpegUtil.getVideoTime("/home/free/下载/a.mp4"));
	}
}
