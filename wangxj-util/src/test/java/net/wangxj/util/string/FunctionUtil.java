package net.wangxj.util.string;

import java.util.regex.Pattern;

import net.wangxj.util.html.HTMLUtil;

/**
 * @author huoshan
 * created by 2017年7月4日 下午4:19:33
 * 
 */
public class FunctionUtil {   
    public static void main(String[] args) {   
        String str = "<h2>测试</h2><p>2017-07-04 16:27:56.476 DEBUG 11689 ---"
        		+ " [ &nbsp;restartedMain] m.m.MbeansDescriptorsIntrospectionSource : "
        		+ "Introspected attribute processingTime public synchronized long "
        		+ "org.apache.coyote.RequestGroupInfo.getProcessingTime() public synchronized "
        		+ "void org.apache.coyote.RequestGroupInfo.setProcessingTime(long)"
        		+ "</p><p>2017-07-04 16:27:56.477 DEBUG 11689 --- [ &nbsp;restartedMain] "
        		+ "m.m.MbeansDescriptorsIntrospectionSource : "
        		+ "Introspected attribute errorCount public synchronized int "
        		+ "org.apache.coyote.RequestGroupInfo.getErrorCount() public synchronized"
        		+ " void org.apache.coyote.RequestGroupInfo.setErrorCount(int)</p>"
        		+ "<p>2017-07-04 16:27:56.477 DEBUG 11689 --- [ &nbsp;restartedMain] "
        		+ "m.m.MbeansDescriptorsIntrospectionSource : "
        		+ "Setting name: org.apache.coyote.RequestGroupInfo</p><p>2017-07-04 16:27:56.477 "
        		+ "DEBUG 11689 --- [ &nbsp;restartedMain] o.a.tomcat.util.modeler.BaseModelMBean &nbsp; "
        		+ ": preRegister org.apache.coyote.RequestGroupInfo@12b5e296 Tomcat:type=GlobalRequestProcessor,"
        		+ "name=\"http-nio-8000\"</p><p>2017-07-04 16:27:56.495 DEBUG 11689 --- "
        		+ "[8000-Acceptor-0] o.apache.tomcat.util.threads.LimitLatch &nbsp;:"
        		+ " Counting up[http-nio-8000-Acceptor-0] latch=0</p><p>2017-07-04 16:27:56.509 &nbsp;INFO 11689 --- "
        		+ "[ &nbsp;restartedMain] s.b.c.e.t.TomcatEmbeddedServletContainer : Tomcat started on port(s): 8000 (http)</p>"
        		+ "<p>2017-07-04 16:27:56.511 DEBUG 11689 --- [ &nbsp;restartedMain] o.s.w.c.s.StandardServletEnvironment &nbsp;"
        		+ " &nbsp; : Adding [server.ports] PropertySource with highest search precedence</p"
        		+ "><p>2017-07-04 16:27:56.515 DEBUG 11689 ---"
        		+ " [ &nbsp;restartedMain] o.s.boot.devtools.restart.Restarter &nbsp; &nbsp;"
        		+ " &nbsp;: Creating new Restarter for thread Thread[main,5,main]</p>"
        		+ "<p>2017-07-04 16:27:56.515 DEBUG 11689 --- [ &nbsp;restartedMain]"
        		+ " o.s.boot.devtools.restart.Restarter &nbsp; &nbsp; &nbsp;: Immediately restarting application</p>"
        		+ "<p>2017-07-04 16:27:56.515 DEBUG 11689 --- [ &nbsp;restartedMain]"
        		+ " o.s.boot.devtools.restart.Restarter &nbsp; &nbsp; &nbsp;: Created"
        		+ " RestartClassLoader org.springframework.boot.devtools.restart.classloader.R"
        		+ "estartClassLoader@66efc046</p><p>2017-07-04 16:27:56.515 DEBUG 11689 --- "
        		+ "[ &nbsp;restartedMain] o.s.boot.devtools.restart.Restarter &nbsp; &nbsp; "
        		+ "&nbsp;: Starting application net.codeblog.CodeblogApplication with URLs "
        		+ "[file:/home/huoshan/gitRespo/codeblog/target/classes/,"
        		+ " file:/home/huoshan/gitRespo/wangxj-util/wangxj-util/target/classes/]</p>"
        		+ "<p>2017-07-04 16:27:56.516 &nbsp;INFO 11689 --- [ &nbsp;restartedMain] net.codeblog.CodeblogApplication &nbsp;"
        		+ " &nbsp; &nbsp; &nbsp; : Started CodeblogApplication in 10.604 seconds (JVM running for 11.198)</p><p>"
        		+ "<img alt=\"image.png\" src=\"http://image.localcodeblog.net/images/blog/00e48db0cfb44b1d9a50edd68bea0b4c/dabd1cafcc644889a8e908e104a888f7/597a5b9d123943a5ad53cfa574d2b2c8.png\" width=\"1232\" height=\"559\"><br></p><p>测试</p>";
        
       String test =  "<blockquote><p>&nbsp;继承与多态<img alt=\"2017-01-11 11-20-46屏幕截图.png\" src=\"http://image.localcodeblog.net/images/blog/00e48db0cfb44b1d9a50edd68bea0b4c/e4faaf5b841a41b3ab8c66dfbb658b6a/6f2f91c43ad24ebc826a1057294fc5ad.png\" width=\"1766\" height=\"936\"></p></blockquote>";
        String str_text = HTMLUtil.Html2Text(str);   
        System.out.println(str_text);   
        String slice =  HTMLUtil.abbreviate(str_text, 100, "...");   
        System.out.println(slice);   
    }   
  
   
}
