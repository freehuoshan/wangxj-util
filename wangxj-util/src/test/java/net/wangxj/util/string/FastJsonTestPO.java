/**
 * author: wangxj
 * create time: 上午7:58:35
 */
package net.wangxj.util.string;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author huoshan
 * created by 2017年5月14日 上午7:58:35
 * 
 */
public class FastJsonTestPO {

	@JSONField(name="test_uuid")
	private String testUuid;
	private String testName;
	
	public FastJsonTestPO(String testUuid, String testName) {
		super();
		this.testUuid = testUuid;
		this.testName = testName;
	}

	public String getTestUuid() {
		return testUuid;
	}

	public void setTestUuid(String testUuid) {
		this.testUuid = testUuid;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	@Override
	public String toString() {
		return "FastJsonTest [testUuid=" + testUuid + ", testName=" + testName + "]";
	}
	
	
	
}
