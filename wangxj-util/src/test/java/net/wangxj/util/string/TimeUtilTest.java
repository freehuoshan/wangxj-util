package net.wangxj.util.string;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.math3.util.Precision;
import org.junit.Test;

public class TimeUtilTest {
	
	@Test
	public void testTime() throws ParseException{
		String befor = "2018-01-14 11:41:23";
		DateFormat fmt =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = fmt.parse(befor);
		String after = TimeUtil.addTime(befor, null, null, null, 24);
		System.out.println(after);
		Date afterDate = fmt.parse(after);
		System.out.println(new Date());
		System.out.println(new Date().before(afterDate));
	}
	
	@Test
	public void testDecimal(){
		float price=30.0f;
		DecimalFormat decimalFormat=new DecimalFormat(".00");//构造方法的字符格式这里如果小数不足2位,会以0补足.
		String p=decimalFormat.format(price);//format 返回的是字符串
		System.out.println(new Float(p));
		
		float price1=89.00f;
		
		float num=(float)(Math.round(price1*100)/100);
		System.out.println(num);
	}
	
	@Test
	public void testGetRandomTime(){
		for(int len = 100 ; len > 0 ; len--){
			System.out.println(TimeUtil.randomTime());
		}
	}
	
	@Test
	public void testTimeDiff() throws InterruptedException{
		long start = System.currentTimeMillis();
		Thread.sleep(3000);
		long end = System.currentTimeMillis();
		System.out.println(TimeUtil.TimeDifference(start, end));
	}
	
}
