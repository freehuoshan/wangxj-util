package net.wangxj.util.string;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import org.apache.commons.io.FilenameUtils;
import org.junit.Test;

public class UuidUtilTest implements Callable {
	
	@Override  
    public Object call() throws Exception {//覆盖call()方法，并将结果返回，该结果就是FutureTask的get()获得值  
        Set<String> sets = new HashSet<String>();  
        for(int i=0;i<1000;i++){  
            sets.add(UuidUtil.newGUID());  
        }  
        return sets;  
    }

    @Test
    public void testRandom(){
      System.out.println("abc".split("&"));
    }

	/**
	 * 测试uuidUtil是否会生成重复值
	 */
	@Test
	public void testUuid(){
		List<FutureTask<Set<String>>> futureTasks = new ArrayList<FutureTask<Set<String>>>();//为了后面统计这些FutureTask的结果准备的  
        ExecutorService pool = Executors.newFixedThreadPool(5);//创建一个拥有5个线程的线程池，其大小起码要小于等于futureTasks的吧，否则不就浪费了吗  
        for(int i=0;i<1000;i++){  
            FutureTask<Set<String>> futureTask = new FutureTask<Set<String>>(new UuidUtilTest());//可以跳到GenUUId的类，看其实现  
            futureTasks.add(futureTask);//便于下面的for循环统计结果  
            pool.submit(futureTask);//将FutureTask提交到线程池中，有子线程执行  
        }  
  
        Set<String> sets = new HashSet<String>();  
        for(FutureTask<Set<String>> futureTask:futureTasks){  
            try{  
                sets.addAll(futureTask.get());//统计结果  
            }catch (Exception e){  
               e.printStackTrace();
            }  
        }  
        pool.shutdown();  
        for(String str:sets){  
            System.out.println(str);  
        }  
        System.out.println("多线程计算后的总结果是:" + sets.size());  
    }  
  
}	
