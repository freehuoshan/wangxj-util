/**
 * author: wangxj
 * create time: 上午8:01:07
 */
package net.wangxj.util.string;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;

/**
 * @author huoshan
 * created by 2017年5月14日 上午8:01:07
 * 
 */
public class FastJsonTest {
	
	@Test
	public void testFastJson(){
		Map<String,Object> jsonMap = new HashMap<>();
		jsonMap.put("obj", new FastJsonTestPO("123", "testname"));
		jsonMap.put("aa", "bb");
		String jsonString = JSONObject.toJSONString(jsonMap);
		System.out.println(jsonString);
		
		String json = "{\"testName\":\"testname\",\"testUuid\":\"123\"}";
		
		System.out.println((FastJsonTestPO)JSONObject.parse(json));
				
	}

	@Test
	public void testStr(){
		System.out.println("".trim());
	}

}
