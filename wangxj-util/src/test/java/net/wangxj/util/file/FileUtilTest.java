package net.wangxj.util.file;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.wangxj.util.string.StringUtil;
import org.apache.commons.io.FilenameUtils;
import org.junit.Before;
import org.junit.Test;

public class FileUtilTest {
	
	private FileUtil fileUtil;
	
	@Before
	public void before(){
		fileUtil = new FileUtil();
	}
	
	@Test
	public void testGetSuffix(){
		System.out.println(FileUtil.getFileSuffix("a.txt"));
	}
	
	@Test
	public void testUuid(){
		List<String> nameList = new ArrayList<>();
		for(int len =200; len >0 ; len--){
			String mkFileName = FileUtil.mkFileName("aa.txt");
			nameList.add(mkFileName);
			if(nameList.contains(mkFileName)){
				System.out.println("生成重值!!!");
			}
			
			System.out.println(mkFileName);
		}
		
		System.out.println("完成");
	}
	
	@Test
	public void testMakeFilePath()
	{
		String mkFilePath = FileUtil.mkFilePath("/home/huoshan/", "b.txt");
		System.out.println(mkFilePath);
	}
	
	@Test
	public void testGetFileName(){
		System.out.println(FileUtil.getFileNameByPathUrl("a.txt"));
	}
	
	@Test
	public void testGetSuffixByPathUrl(){
		System.out.println(FileUtil.getSuffixByPathUrl("/home/huoshan/a/b/c.txt"));
	}
	
	@Test
	public void testGenerateAllPath(){
		System.out.println(FileUtil.generteUuidFileAllPath("/home/huoshan/", "a.txt"));
	}
	
	
	@Test
	public void compressFile() throws IOException{
		String folder = "/media/huoshan/Seagate Backup Plus Drive/我的文件/秀人网/test/673-翟奥奥baby-居家|俏丽|制服-2017.01.03-09:23:04-翟奥奥baby-XIUREN-1-70-jpg-yes";
		String zipFile = "/media/huoshan/Seagate Backup Plus Drive/我的文件/秀人网/test/673-翟奥奥baby-居家|俏丽|制服-2017.01.03-09:23:04-翟奥奥baby-XIUREN-1-70-jpg-yes.zip";
		FileUtil.compress(folder, zipFile);
	}

	@Test
	public void testFilenameutil()throws Exception{

		FileUtil.unZipFiles(new File("/media/free/39908A705562C090/Downloads/611224826_品味生活偏执/apps/baidu_shurufa/beautyleg/zip/a.zip"), "/home/free/下载");

	}

	@Test
	public void testFileReplace() throws IOException {
		String[] methods = {"update(\"0x0\", \")yZ[\")", "bj", "l", "length", "substr", "push", " ", "split", "a", "join", "", "charCodeAt", "fromCharCode", "update(\"0x1\", \"bKHS\")", "match", "f", "i", "h", "g", "apply", "j", "c", "0", "33676837615472503871743947355167613353386d6449304f", "reverse", "clientX", "clientY", "event", "getElementById", "left", "getBoundingClientRect", "floor", "update(\"0x2\", \"Db0A\")", "offsetWidth", "offsetHeight", "update(\"0x3\", \"78B#\")", "createObjectURL", "userAgent", "test", "location",
				"hpjav.tv", "indexOf", "host", "href", "attr", "update(\"0x4\", \"^zpH\")", "1", "vid=", "w", "each", "update(\"0x5\", \"X&Xt\")", "update(\"0x6\", \"C3)#\")", "click", "#download_div", "ID", "update(\"0x7\", \"jA4P\")", "update(\"0x8\", \"CrZU\")", "update(\"0x9\", \"WTDa\")", "update(\"0xa\", \"X&Xt\")", "addClass", "no", "data-type", "#JKDiv_", "#btn", "undefined", "update(\"0xb\", \"VD#f\")", "url(/images/ajaximg.gif)", "update(\"0xc\", \"Ue)a\")", "update(\"0xd\", \"^zpH\")", "no-repeat", "background-position", "center", "update(\"0xe\", \"VhLF\")", "64px 64px",
				"about:blank", "<iframe id=\"server', '\" src=\"\"", "update(\"0xf\", \"zsMc\")", "html", "document", "update(\"0x10\", \"WTDa\")", "server", "<script type=\"text/javascript\">location.href = ", ";\\x3c/script>", "update(\"0x11\", \"MmXC\")", "write", "close", "#JKDiv_0", "background-image", "update(\"0x12\", \"&PJU\")", "64px 64px,contain", "iframe", "createElement", "update(\"0x13\", \"BOrH\")", "setAttribute", "width", "100%", "height", "allowfullscreen", "true", "update(\"0x14\", \"MmXC\")", "mozallowfullscreen", "src", "id", "server0",
				".play-button", "size", "update(\"0x15\", \"VD#f\")", "update(\"0x16\", \"totg\")", "btn", "#", "update(\"0x17\", \"^zpH\")", "remove", "#server", "style", "JKDiv_", "display", "none", "className", "btn btn-warning", "block", "btn btn-warning active"};

		for (int i = 0; i< methods.length; i++) {
			String src = "methods\\[".concat(String.valueOf(i)).concat("\\]");
			String dest = methods[i];
			System.out.println("替换" + src + "为->" + dest);
			FileUtil.updateFile("/home/free/下载/js.txt",src , dest);
		}


	}

}
