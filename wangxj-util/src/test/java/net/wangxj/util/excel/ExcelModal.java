package net.wangxj.util.excel;

import java.util.Date;

import com.github.sd4324530.fastexcel.annotation.MapperCell;

public class ExcelModal {
	
	@MapperCell(cellName="Payment_Type")
    private String Payment_Type;        
	@MapperCell(cellName="Transaction_Type")
    private String Transaction_Type;     
	@MapperCell(cellName="Transaction_Amount")
    private Double Transaction_Amount;    	
	@MapperCell(cellName="Transaction_Date")
    private Date Transaction_Date; 		
	@MapperCell(cellName="Transaction_Received_Date")
    private Date Transaction_Received_Date;  
	@MapperCell(cellName="Batch_Type")
    private String Batch_Type;   			
	@MapperCell(cellName="Is_Batch_Closed")
    private Boolean Is_Batch_Closed;             
	@MapperCell(cellName="Batch_Close_Date")
    private Date Batch_Close_Date;                   
	@MapperCell(cellName="Disposition_Status")
    private String Disposition_Status;             
	@MapperCell(cellName="MLink_Processed")
    private Boolean MLink_Processed;                 
	@MapperCell(cellName="MLiNk_Response_Code")
    private String MLiNk_Response_Code;     
	@MapperCell(cellName="Pend_RTC_Table")
    private Boolean Pend_RTC_Table;         
	@MapperCell(cellName="Load_Archive_Table")
    private Boolean Load_Archive_Table;     
	@MapperCell(cellName="Load_IHUB_AUTH_Table")
    private Boolean Load_IHUB_AUTH_Table;	
	@MapperCell(cellName="Load_IHUB_TXN_Table")
    private Boolean Load_IHUB_TXN_Table;	
	@MapperCell(cellName="Clearing_File")
    private Boolean Clearing_File;          
	@MapperCell(cellName="Settlement_File")
    private Boolean Settlement_File;        
	@MapperCell(cellName="Reject_File")
    private Boolean Reject_File;            
	@MapperCell(cellName="Is_Priced")
    private Boolean Is_Priced;
    
    
	public String getPayment_Type() {
		return Payment_Type;
	}
	public void setPayment_Type(String payment_Type) {
		Payment_Type = payment_Type;
	}
	public String getTransaction_Type() {
		return Transaction_Type;
	}
	public void setTransaction_Type(String transaction_Type) {
		Transaction_Type = transaction_Type;
	}
	public Double getTransaction_Amount() {
		return Transaction_Amount;
	}
	public void setTransaction_Amount(Double transaction_Amount) {
		Transaction_Amount = transaction_Amount;
	}
	public Date getTransaction_Date() {
		return Transaction_Date;
	}
	public void setTransaction_Date(Date transaction_Date) {
		Transaction_Date = transaction_Date;
	}
	public Date getTransaction_Received_Date() {
		return Transaction_Received_Date;
	}
	public void setTransaction_Received_Date(Date transaction_Received_Date) {
		Transaction_Received_Date = transaction_Received_Date;
	}
	public String getBatch_Type() {
		return Batch_Type;
	}
	public void setBatch_Type(String batch_Type) {
		Batch_Type = batch_Type;
	}
	public Boolean getIs_Batch_Closed() {
		return Is_Batch_Closed;
	}
	public void setIs_Batch_Closed(Boolean is_Batch_Closed) {
		Is_Batch_Closed = is_Batch_Closed;
	}
	public Date getBatch_Close_Date() {
		return Batch_Close_Date;
	}
	public void setBatch_Close_Date(Date batch_Close_Date) {
		Batch_Close_Date = batch_Close_Date;
	}
	public String getDisposition_Status() {
		return Disposition_Status;
	}
	public void setDisposition_Status(String disposition_Status) {
		Disposition_Status = disposition_Status;
	}
	public Boolean getMLink_Processed() {
		return MLink_Processed;
	}
	public void setMLink_Processed(Boolean mLink_Processed) {
		MLink_Processed = mLink_Processed;
	}
	public String getMLiNk_Response_Code() {
		return MLiNk_Response_Code;
	}
	public void setMLiNk_Response_Code(String mLiNk_Response_Code) {
		MLiNk_Response_Code = mLiNk_Response_Code;
	}
	public Boolean getPend_RTC_Table() {
		return Pend_RTC_Table;
	}
	public void setPend_RTC_Table(Boolean pend_RTC_Table) {
		Pend_RTC_Table = pend_RTC_Table;
	}
	public Boolean getLoad_Archive_Table() {
		return Load_Archive_Table;
	}
	public void setLoad_Archive_Table(Boolean load_Archive_Table) {
		Load_Archive_Table = load_Archive_Table;
	}
	public Boolean getLoad_IHUB_AUTH_Table() {
		return Load_IHUB_AUTH_Table;
	}
	public void setLoad_IHUB_AUTH_Table(Boolean load_IHUB_AUTH_Table) {
		Load_IHUB_AUTH_Table = load_IHUB_AUTH_Table;
	}
	public Boolean getLoad_IHUB_TXN_Table() {
		return Load_IHUB_TXN_Table;
	}
	public void setLoad_IHUB_TXN_Table(Boolean load_IHUB_TXN_Table) {
		Load_IHUB_TXN_Table = load_IHUB_TXN_Table;
	}
	public Boolean getClearing_File() {
		return Clearing_File;
	}
	public void setClearing_File(Boolean clearing_File) {
		Clearing_File = clearing_File;
	}
	public Boolean getSettlement_File() {
		return Settlement_File;
	}
	public void setSettlement_File(Boolean settlement_File) {
		Settlement_File = settlement_File;
	}
	public Boolean getReject_File() {
		return Reject_File;
	}
	public void setReject_File(Boolean reject_File) {
		Reject_File = reject_File;
	}
	public Boolean getIs_Priced() {
		return Is_Priced;
	}
	public void setIs_Priced(Boolean is_Priced) {
		Is_Priced = is_Priced;
	}
	
	@Override
	public String toString() {
		return "ExcelModal [Payment_Type=" + Payment_Type + ", Transaction_Type=" + Transaction_Type
				+ ", Transaction_Amount=" + Transaction_Amount + ", Transaction_Date=" + Transaction_Date
				+ ", Transaction_Received_Date=" + Transaction_Received_Date + ", Batch_Type=" + Batch_Type
				+ ", Is_Batch_Closed=" + Is_Batch_Closed + ", Batch_Close_Date=" + Batch_Close_Date
				+ ", Disposition_Status=" + Disposition_Status + ", MLink_Processed=" + MLink_Processed
				+ ", MLiNk_Response_Code=" + MLiNk_Response_Code + ", Pend_RTC_Table=" + Pend_RTC_Table
				+ ", Load_Archive_Table=" + Load_Archive_Table + ", Load_IHUB_AUTH_Table=" + Load_IHUB_AUTH_Table
				+ ", Load_IHUB_TXN_Table=" + Load_IHUB_TXN_Table + ", Clearing_File=" + Clearing_File
				+ ", Settlement_File=" + Settlement_File + ", Reject_File=" + Reject_File + ", Is_Priced=" + Is_Priced
				+ "]";
	}                        
	
}
