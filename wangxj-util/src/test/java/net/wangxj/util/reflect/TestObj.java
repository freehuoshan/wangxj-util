package net.wangxj.util.reflect;

/**
 * @author huoshan
 * created by 2017年5月28日 下午1:45:24
 * 
 */
public class TestObj {
	
	private Integer age;
	private String name;
	private Integer gender;
	
	public TestObj() {
		super();
	}

	public TestObj(Integer age, String name, Integer gender) {
		super();
		this.age = age;
		this.name = name;
		this.gender = gender;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "TestObj [age=" + age + ", name=" + name + ", gender=" + gender + "]";
	}
	
	

}
