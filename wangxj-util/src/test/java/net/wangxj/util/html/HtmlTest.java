package net.wangxj.util.html;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.impl.conn.LoggingSessionOutputBuffer;
import org.junit.Test;

import net.wangxj.util.file.FileUtil;
import net.wangxj.util.string.StringUtil;
import net.wangxj.util.string.UuidUtil;

/**
 * @author huoshan
 * created by 2017年11月8日 下午4:47:46
 * 
 */
public class HtmlTest {
	
	@Test
	public void testHtmlToText() throws IOException{
		 this.readFileByLine("/home/free/下载/english.srt","/home/free/下载/english.txt");
	}
	
	public void readFileByLine(String pathname,String outputFile) throws IOException{
		FileReader reader = new FileReader(pathname);
		BufferedReader bufferReader = new BufferedReader(reader);
		
		String fileText = "";
		String line = "";
		while((line = bufferReader.readLine()) != null){
			if(StringUtil.isBlankOrNull(line)){
				continue;
			}
			char firstChar = line.charAt(0);
			if(!Character.isDigit(firstChar)){
				line = HTMLUtil.Html2Text(line);
				fileText += line;
				fileText += "\n";
			}
		}
		
		File file = new File(outputFile);  
        PrintStream ps = new PrintStream(new FileOutputStream(file));  
        ps.println(fileText);// 往文件里写入字符串  
		
	}
	
	public boolean isNumeric(String str){ 
		   Pattern pattern = Pattern.compile("[0-9]*"); 
		   Matcher isNum = pattern.matcher(str);
		   if( !isNum.matches() ){
		       return false; 
		   } 
		   return true; 
		}
	
	
	

}
