/**
 * author: wangxj
 * create time: 上午11:31:48
 */
package net.wangxj.util.annotation;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import net.wangxj.util.annotation.util.NotRepeatUtil;

/**
 * @author huoshan
 * created by 2017年5月6日 上午11:31:48
 * 
 */
public class NotRepeatTest {
	
	@Test
	public void testNotRepeat() throws NoSuchMethodException, SecurityException, ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException{
		
		TestPo testPo = new TestPo(1, "name", "gender");
		
		Object newTestPo = NotRepeatUtil.buildObjFromAnnotatedField(testPo);
		System.out.println(newTestPo);
		
	}
	
}
