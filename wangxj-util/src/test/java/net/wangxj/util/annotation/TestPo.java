/**
 * author: wangxj
 * create time: 上午11:32:20
 */
package net.wangxj.util.annotation;

/**
 * @author huoshan
 * created by 2017年5月6日 上午11:32:20
 * 
 */
public class TestPo {
	
	private Integer age;
	@NotRepeat
	private String name;
	private String gender;
	
	public TestPo(Integer age, String name, String gender) {
		super();
		this.age = age;
		this.name = name;
		this.gender = gender;
	}

	public TestPo() {
		super();
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "TestPo [age=" + age + ", name=" + name + ", gender=" + gender + "]";
	}
	
}
