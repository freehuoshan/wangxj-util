package net.wangxj.util.encry;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.validation.constraints.AssertTrue;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author huoshan
 * created by 2017年5月19日 下午4:01:25
 * 
 */
public class EncryTest {
	
	@Test
	public void testPBKF2SHA1() throws UnsupportedEncodingException, Exception{
		
		
		String str = "abcdefghijklmnopqrstuvwxyz";
		for(int i = 26 ; i > 0 ; i--){
			String str1 = str.substring(i);
			String md5Str = new MD5().getMD5ofStr(str1);
			String encryedPass = PBKDF2SHA1.generateStorngPasswordHash(md5Str);
			String base64EncryPass = Base64Utill.encode(encryedPass);
			
			System.out.println("str-->" + str1 + "-------length:" + str1.length());
			System.out.println("\nmd5-->" + md5Str + "-------length:" + md5Str.length()); 
			System.out.println("\npbkdf2Hash1:" + encryedPass + "-------length:" +encryedPass.length());
			System.out.println("\nbaseedPass:" + base64EncryPass + "-------length:" + base64EncryPass.length() + "\n\n");
		}
		
	}
	
	
	
	@Test
	public void testBaseLength() throws UnsupportedEncodingException, Exception{
		String str = "abcdefghijklmnopqrstuvwxyz";
		for(int i = 25 ; i > 0 ; i--){
			String str1 = str.substring(i);
			String enStr = Base64Utill.encode(str1);
			System.out.println(str1 + "--" + enStr + "--" + enStr.length());
		}
	}
	
	@Test
	public void testMd5() throws UnsupportedEncodingException{
//		String str = "abcdefghijklmnopqrstuvwxyz";
//		for(int i = 25 ; i > 0 ; i--){
//			String str1 = str.substring(i);
			
//			System.out.println(str1 + "--" + MD5Util.encodePassword(str1).equals(MD5.getMD5ofStr(str1).toLowerCase()));
//			Assert.assertTrue(MD5Util.encodePassword(str1).equals(MD5.getMD5ofStr(str1).toLowerCase()));
//		}
//		orderid + orderuid + paysapi_id + price + realprice + token

//		String str = "373f8ecb4782401fb9efead21381e58fd999227e94a74969b179f2d8fd5225751234567890abcdefghij1235888.00888.00688251035bd749ea134ecdc1701dcb29";
//		System.out.println(MD5.getMD5ofStr(str).toLowerCase());
//		System.out.println(MD5Util.encodePassword(str));
//		System.out.println(MD5.getMD5ofStr(str).toLowerCase().equals(MD5Util.encodePassword(str)));

		System.out.println(MD5.getMD5ofStr("wxj1021"));
	}
	
	@Test 
	public void testAbc() throws NoSuchAlgorithmException, InvalidKeySpecException, UnsupportedEncodingException{
//		String pb2 = "690:5b42403161323139333564:4f1be6ec75431b5b35f9c3fb20c649b567987a2904b5e552dd404bb4d08af2d0a3c570a428ea6139328ec21879c1c87e652ecd56040d22187f9079994353c64e";
//		System.out.println(PBKDF2SHA1.validatePassword("wxj123", pb2));
		String md5Str = MD5Util.encodePassword("abc123").toUpperCase();
		System.out.println(md5Str);
		String pb3 = PBKDF2SHA1.generateStorngPasswordHash(md5Str.toUpperCase());
		System.out.println(pb3);
		System.out.println(PBKDF2SHA1.validatePassword(md5Str, pb3));
	}


}
