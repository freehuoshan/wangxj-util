package net.wangxj.util.jersey;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.glassfish.jersey.media.multipart.Boundary;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;

/**
 * @author huoshan
 * created by 2017年6月4日 下午1:37:54
 * 
 */
public class JerseyClient {
	
	public static Logger log = Logger.getLogger(JerseyClient.class);

	/**
	 * 使用连接池，不适用于短连接
	 * @param method
	 * @param requestUrl
	 * @param pathList
	 * @param headParams
	 * @param queryParams
	 * @param requestData
	 * @return
	 */
	public static String rest(RequestMethod method ,String requestUrl , List<String> pathList,
								Map<String,Object> headParams,
								Map<String,Object> queryParams,
								String requestData){
		
		Client client = HttpConnectPool.newClient(null);
		Builder builder = buildData(client, method, requestUrl, pathList, headParams, queryParams, requestData);
		return (String) send(builder, method, requestData, "string");
	}

	public static String restProxy(RequestMethod method ,String requestUrl , List<String> pathList,
							  Map<String,Object> headParams,
							  Map<String,Object> queryParams,
							  String requestData, String proxyAddress)throws Exception{

		Client client = HttpConnectPool.newClient(proxyAddress);
		Builder builder = buildData(client, method, requestUrl, pathList, headParams, queryParams, requestData);
		return (String) send(builder, method, requestData, "string");
	}

	/**
	 * 使用连接池，不适用于短连接
	 * @param method
	 * @param requestUrl
	 * @param pathList
	 * @param headParams
	 * @param queryParams
	 * @param requestData
	 * @return
	 */
	public static Response restResponse(RequestMethod method ,String requestUrl , List<String> pathList,
										Map<String,Object> headParams,
										Map<String,Object> queryParams,
										String requestData){
		Client client = HttpConnectPool.newClient(null);
		Builder builder = buildData(client, method, requestUrl, pathList, headParams, queryParams, requestData);
		return (Response) send(builder, method, requestData, "response");
	}

	public static Response restResponseProxy(RequestMethod method ,String requestUrl , List<String> pathList,
										Map<String,Object> headParams,
										Map<String,Object> queryParams,
										String requestData,
										String proxyAddress)throws Exception{
		Client client = HttpConnectPool.newClient(proxyAddress);
		Builder builder = buildData(client, method, requestUrl, pathList, headParams, queryParams, requestData);
		return (Response) send(builder, method, requestData, "response");
	}



	/**
	 * 不使用连接池，适用于短连接
	 * @param method
	 * @param requestUrl
	 * @param pathList
	 * @param headParams
	 * @param queryParams
	 * @param requestData
	 * @return
	 */
	public static String restNoManager(RequestMethod method ,String requestUrl , List<String> pathList,
									   Map<String,Object> headParams,
									   Map<String,Object> queryParams,
									   String requestData){

		Client client = ClientBuilder.newClient();
		Builder builder = buildData(client, method, requestUrl, pathList, headParams, queryParams, requestData);
		return (String) send(builder, method, requestData, "string");
	}

	/**
	 * 上传文件
	 * @param requestUrl
	 * @param fileMap
	 * @param headParams
	 * @return
	 */
	public static String restFile(String requestUrl, Map<String, File> fileMap, Map<String, Object> headParams, String proxAddress){
		MultiPart multiPart = null;
		try{
			Client client = HttpConnectPool.newMultipartClient(proxAddress);
			WebTarget webTarget = client.target(requestUrl);

			if(fileMap != null){
				multiPart = new MultiPart();
				for(Map.Entry<String, File> entry : fileMap.entrySet()){
					FileDataBodyPart bodyPart = new FileDataBodyPart(entry.getKey(), entry.getValue(), MediaType.APPLICATION_OCTET_STREAM_TYPE);
					multiPart.bodyPart(bodyPart);
				}
			}
			Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE);
			//构造请求头
			if(headParams != null){
				for(Map.Entry<String, Object> entry : headParams.entrySet()){
					invocationBuilder.header(entry.getKey(), entry.getValue());
				}
			}
			MediaType contentType = MediaType.MULTIPART_FORM_DATA_TYPE;
			contentType = Boundary.addBoundary(contentType);
			Response response = invocationBuilder.post(Entity.entity(multiPart, contentType), Response.class);

			if(response.getStatus() == 200){
				return response.readEntity(String.class);
			}else{
				log.error("上传文件失败----error----error" + response.readEntity(String.class));
			}
		}catch (Exception e) {
			System.out.println("Exception has occured "+ e.getMessage());
		} finally {
			if (null != multiPart) {
				try {
					multiPart.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	/**
	 * 获取　builder
	 * @param client
	 * @param method
	 * @param requestUrl
	 * @param pathList
	 * @param headParams
	 * @param queryParams
	 * @param requestData
	 * @return
	 */
	public static Builder buildData(Client client, RequestMethod method ,String requestUrl , List<String> pathList,
						  Map<String,Object> headParams,
						  Map<String,Object> queryParams,
						  String requestData){
		URI uri;
		try{
			URL url = new URL(requestUrl);
			uri = new URI(url.getProtocol(), url.getHost(), url.getPath(), url.getQuery(), null);
		}catch (Exception e){
			log.error("发生错误-->" + e.getMessage());
			e.printStackTrace();
			return null;
		}

		WebTarget webTarget = client.target(uri);
		if(pathList != null){
			for (String path : pathList) {
				webTarget = webTarget.path(path);
			}
		}
		//构造查询参数
		if(queryParams != null){
			for (Map.Entry<String, Object> entry : queryParams.entrySet()) {
				webTarget = webTarget.queryParam(entry.getKey(), entry.getValue());
			}
		}

		Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		//构造请求头
		if(headParams != null){
			for(Map.Entry<String, Object> entry : headParams.entrySet()){
				invocationBuilder.header(entry.getKey(), entry.getValue());
			}
		}
		return invocationBuilder;
	}


	/**
	 * 发送请求获取响应
	 * @param builder
	 * @param method
	 * @param requestData
	 * @param returnType
	 * @return
	 */
	public static Object send(Builder builder,
								RequestMethod method,
								String requestData,
								String returnType){
		//发起请求
		Response response;
		Entity<String> entity;
		switch (method) {
			case GET:
				response = builder.get();
				return getResponse(response, returnType);
			case POST:
				entity = Entity.entity(requestData, MediaType.APPLICATION_JSON);
				response = builder.post(entity);
				return getResponse(response, returnType);
			case PUT:
				entity = Entity.entity(requestData, MediaType.APPLICATION_JSON);
				response = builder.put(entity);
				return getResponse(response, returnType);
			case DELETE:
				response = builder.delete();
				return getResponse(response, returnType);
			default:
				return null;
		}
	}

	public static Object getResponse(Response response, String returnType){
		if("string".equals(returnType)){
			return response.readEntity(String.class);
		}else{
			return response;
		}
	}
	

	


	

	
}
