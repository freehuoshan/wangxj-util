package net.wangxj.util.jersey;

/**
 * @author huoshan
 * created by 2017年5月26日 下午4:52:43
 * 
 */
public enum RequestMethod {
	GET,POST,DELETE,PUT
}
