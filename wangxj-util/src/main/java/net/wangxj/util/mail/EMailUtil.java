package net.wangxj.util.mail;

import javax.mail.MessagingException;
/**
 * @author huoshan
 * created by 2017年7月10日 下午2:50:32
 * 
 */
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.net.smtp.SMTPClient;
import org.apache.commons.net.smtp.SMTPReply;
import org.apache.log4j.Logger;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.Record;
import org.xbill.DNS.Type;

import com.sun.mail.util.MailSSLSocketFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.Properties;

public class EMailUtil {
	
	public static Logger log = Logger.getLogger(EMailUtil.class);

    // 发件人的 邮箱 和 密码（替换为自己的邮箱和密码）
    // PS: 某些邮箱服务器为了增加邮箱本身密码的安全性，给 SMTP 客户端设置了独立密码（有的邮箱称为“授权码”）, 
    //     对于开启了独立密码的邮箱, 这里的邮箱密码必需使用这个独立密码（授权码）。
    
    // 发件人邮箱的 SMTP 服务器地址, 必须准确, 不同邮件服务器地址不同, 一般(只是一般, 绝非绝对)格式为: smtp.xxx.com
    // 网易163邮箱的 SMTP 服务器地址为: smtp.163.com

    // 收件人邮箱（替换为自己知道的有效邮箱）
	/**
	 * 发送邮件（使用smtp协议）
	 * @param sendMailAcount	发送邮箱帐号
	 * @param sendMailPassword  发送邮箱密码
	 * @param sendMailSMTPHost	发送邮箱服务器地址
	 * @param reveiveMailAcount	接收邮箱帐号
	 * @param smtpPort			发送邮箱服务器smtp端口
	 * @param emailTitle		邮件标题
	 * @param emailContent		邮件内容
	 * @param sendEmailAlais	发送邮箱别名
	 * @throws Exception
	 */
    public static void sendMessage(String sendMailAcount , String sendMailPassword,String sendMailSMTPHost,String reveiveMailAcount,
    		String smtpPort,String emailTitle,String emailContent,String sendEmailAlais) throws Exception{
    	// 1. 创建参数配置, 用于连接邮件服务器的参数配置
        Properties props = new Properties();                    // 参数配置
        props.setProperty("mail.transport.protocol", "smtp");   // 使用的协议（JavaMail规范要求）
        props.setProperty("mail.smtp.host", sendMailSMTPHost);   // 发件人的邮箱的 SMTP 服务器地址
        props.setProperty("mail.smtp.auth", "true");            // 需要请求认证
        
        // PS: 某些邮箱服务器要求 SMTP 连接需要使用 SSL 安全认证 (为了提高安全性, 邮箱支持SSL连接, 也可以自己开启),
        //     如果无法连接邮件服务器, 仔细查看控制台打印的 log, 如果有有类似 “连接失败, 要求 SSL 安全连接” 等错误,
        //     打开下面 /* ... */ 之间的注释代码, 开启 SSL 安全连接。
        // SMTP 服务器的端口 (非 SSL 连接的端口一般默认为 25, 可以不添加, 如果开启了 SSL 连接,
        //                  需要改为对应邮箱的 SMTP 服务器的端口, 具体可查看对应邮箱服务的帮助,
        //                  QQ邮箱的SMTP(SLL)端口为465或587, 其他邮箱自行去查看)
        MailSSLSocketFactory msf;
		try {
			msf = new MailSSLSocketFactory();
			msf.setTrustAllHosts(true);
	        
	        props.setProperty("mail.smtp.port", smtpPort);
	        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	        props.setProperty("mail.smtp.socketFactory.fallback", "false");
	        props.setProperty("mail.smtp.socketFactory.port", smtpPort);
	        props.setProperty("mail.smtp.ssl.enable", "true");
	        props.put("mail.smtp.ssl.socketFactory",msf);
	        log.debug("发送邮件参数:-->" + props);
	        // 2. 根据配置创建会话对象, 用于和邮件服务器交互
	        Session session = Session.getDefaultInstance(props);
	        session.setDebug(true);                                 // 设置为debug模式, 可以查看详细的发送 log

	        // 3. 创建一封邮件
	        MimeMessage message = createMimeMessage(session, sendMailAcount, reveiveMailAcount,emailTitle,emailContent,sendEmailAlais);
	        log.debug("邮件:-->" + message);
	        // 4. 根据 Session 获取邮件传输对象
	        Transport transport = session.getTransport();
	        // 5. 使用 邮箱账号 和 密码 连接邮件服务器, 这里认证的邮箱必须与 message 中的发件人邮箱一致, 否则报错
	        // 
	        //    PS_01: 成败的判断关键在此一句, 如果连接服务器失败, 都会在控制台输出相应失败原因的 log,
	        //           仔细查看失败原因, 有些邮箱服务器会返回错误码或查看错误类型的链接, 根据给出的错误
	        //           类型到对应邮件服务器的帮助网站上查看具体失败原因。
	        //
	        //    PS_02: 连接失败的原因通常为以下几点, 仔细检查代码:
	        //           (1) 邮箱没有开启 SMTP 服务;
	        //           (2) 邮箱密码错误, 例如某些邮箱开启了独立密码;
	        //           (3) 邮箱服务器要求必须要使用 SSL 安全连接;
	        //           (4) 请求过于频繁或其他原因, 被邮件服务器拒绝服务;
	        //           (5) 如果以上几点都确定无误, 到邮件服务器网站查找帮助。
	        //
	        //    PS_03: 仔细看log, 认真看log, 看懂log, 错误原因都在log已说明。
	        transport.connect(sendMailAcount, sendMailPassword);
	        log.debug("开始发送邮件:-->");
	        // 6. 发送邮件, 发到所有的收件地址, message.getAllRecipients() 获取到的是在创建邮件对象时添加的所有收件人, 抄送人, 密送人
	        transport.sendMessage(message, message.getAllRecipients());
	        // 7. 关闭连接
	        transport.close();
	        log.debug("邮件发送成功");
		} catch (Exception e) {
			log.error("发送邮件失败:-->" ,e);
			e.printStackTrace();
			throw e;
		}
    }
    

    /**
     * 创建一封只包含文本的简单邮件
     *
     * @param session 和服务器交互的会话
     * @param sendMail 发件人邮箱
     * @param receiveMail 收件人邮箱
     * @return
     * @throws MessagingException 
     * @throws UnsupportedEncodingException 
     * @throws Exception
     */
    public static MimeMessage createMimeMessage(Session session, String sendMail, String receiveMail,String emailTitle,String emailContent
    		,String sendMailAlais) throws UnsupportedEncodingException, MessagingException{
        // 1. 创建一封邮件
        MimeMessage message = new MimeMessage(session);

        // 2. From: 发件人
        message.setFrom(new InternetAddress(sendMail, sendMailAlais, "UTF-8"));

        // 3. To: 收件人（可以增加多个收件人、抄送、密送）
        message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(receiveMail, null, "UTF-8"));

        // 4. Subject: 邮件主题
        message.setSubject(emailTitle, "UTF-8");

        // 5. Content: 邮件正文（可以使用html标签）
        message.setContent(emailContent, "text/html;charset=UTF-8");

        // 6. 设置发件时间
        message.setSentDate(new Date());

        // 7. 保存设置
        message.saveChanges();

        return message;
    }
    
    /**
     * 验证邮箱有效性
     * @param email 被验证邮箱
     * @param validateEmail 验证邮箱
     * @return 
     */
    public static boolean isEmailValid(String email, String vlidateEmail) {  
        if (!email.matches("[\\w\\.\\-]+@([\\w\\-]+\\.)+[\\w\\-]+")) {  
            log.error("邮箱（"+email+"）校验未通过，格式不对!");  
            return false;  
        }  
        String host = "";  
        String hostName = email.split("@")[1];  
        //Record: A generic DNS resource record. The specific record types   
        //extend this class. A record contains a name, type, class, ttl, and rdata.  
        Record[] result = null;  
        SMTPClient client = new SMTPClient();  
        try {  
            // 查找DNS缓存服务器上为MX类型的缓存域名信息  
            Lookup lookup = new Lookup(hostName, Type.MX);  
            lookup.run();  
            if (lookup.getResult() != Lookup.SUCCESSFUL) {//查找失败  
                log.error("邮箱（"+email+"）校验未通过，未找到对应的MX记录!");  
                return false;  
            } else {//查找成功  
                result = lookup.getAnswers();  
            }  
            //尝试和SMTP邮箱服务器建立Socket连接  
            for (int i = 0; i < result.length; i++) {  
                host = result[i].getAdditionalName().toString();  
                log.debug("SMTPClient try connect to host:"+host);  
                  
                //此connect()方法来自SMTPClient的父类:org.apache.commons.net.SocketClient  
                //继承关系结构：org.apache.commons.net.smtp.SMTPClient-->org.apache.commons.net.smtp.SMTP-->org.apache.commons.net.SocketClient  
                //Opens a Socket connected to a remote host at the current default port and   
                //originating from the current host at a system assigned port. Before returning,  
                //_connectAction_() is called to perform connection initialization actions.   
                //尝试Socket连接到SMTP服务器  
                client.connect(host);  
                //Determine if a reply code is a positive completion response（查看响应码是否正常）.   
                //All codes beginning with a 2 are positive completion responses（所有以2开头的响应码都是正常的响应）.   
                //The SMTP server will send a positive completion response on the final successful completion of a command.   
                if (!SMTPReply.isPositiveCompletion(client.getReplyCode())) {  
                    //断开socket连接  
                    client.disconnect();  
                    continue;  
                } else {  
                    log.debug("找到MX记录:"+hostName);  
                    log.debug("建立链接成功："+hostName);  
                    break;  
                }  
            }  
            log.info("SMTPClient ReplyString:"+client.getReplyString());  
            String[] validalteEmailArr = vlidateEmail.split("@");
            String emailSuffix=validalteEmailArr[1];  
            String emailPrefix=validalteEmailArr[0];  
            String fromEmail = emailPrefix+"@"+emailSuffix;   
            //Login to the SMTP server by sending the HELO command with the given hostname as an argument.   
            //Before performing any mail commands, you must first login.   
            //尝试和SMTP服务器建立连接,发送一条消息给SMTP服务器  
            client.login(emailPrefix);  
            log.debug("SMTPClient login:"+emailPrefix+"...");  
            log.debug("SMTPClient ReplyString:"+client.getReplyString());  
              
            //Set the sender of a message using the SMTP MAIL command,   
            //specifying a reverse relay path.   
            //The sender must be set first before any recipients may be specified,   
            //otherwise the mail server will reject your commands.   
            //设置发送者，在设置接受者之前必须要先设置发送者  
            client.setSender(fromEmail);  
            log.debug("设置发送者 :"+fromEmail);  
            log.debug("SMTPClient ReplyString:"+client.getReplyString());  
  
            //Add a recipient for a message using the SMTP RCPT command,   
            //specifying a forward relay path. The sender must be set first before any recipients may be specified,   
            //otherwise the mail server will reject your commands.   
            //设置接收者,在设置接受者必须先设置发送者，否则SMTP服务器会拒绝你的命令  
            client.addRecipient(email);  
            log.debug("设置接收者:"+email);  
            log.debug("SMTPClient ReplyString:"+client.getReplyString());  
            log.debug("SMTPClient ReplyCode："+client.getReplyCode()+"(250表示正常)");  
            if (250 == client.getReplyCode()) {  
                return true;  
            }  
        } catch (Exception e) {  
            e.printStackTrace();  
        } finally {  
            try {  
                client.disconnect();  
            } catch (IOException e) {  
            }  
        }  
        return false;  
    }  
    

}
