package net.wangxj.util.number;

import java.util.Random;

/**
 * @author huoshan
 * created by 2018年3月21日 上午9:49:17
 * 
 */
public class NumberUtil {
	
	/**
	 * 返回某个范围内的随机数
	 * @param min
	 * @param max
	 * @return
	 */
	public static String getRandom(int min, int max){
	    Random random = new Random();
	    int s = random.nextInt(max) % (max - min + 1) + min;
	    return String.valueOf(s);
	}
	
	public static String setLengthNumber(String source, int length){
		while(source.length() < length){
			source = "0" + source;
		}
		return source;
	}
}
