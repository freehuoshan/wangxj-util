package net.wangxj.util.encry;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * AES加解密
 *
 * Created by yyh on 2015/10/9.
 */
public class AESUtil {

    /**
     * 密钥算法
     */
    private static final String ALGORITHM = "AES";
    /**
     * 加解密算法/工作模式/填充方式
     */
    private static final String ALGORITHM_STR = "AES/ECB/PKCS5Padding";

    /**
     * SecretKeySpec类是KeySpec接口的实现类,用于构建秘密密钥规范
     */
    private SecretKeySpec key;

    public AESUtil(String hexKey) {
        key = new SecretKeySpec(hexKey.getBytes(), ALGORITHM);
    }

    /**
     * AES加密
     * @param data -> aes ->hex -> base64
     * @return
     * @throws Exception
     */
    public String encryptData(String data) throws Exception {
        Cipher cipher = Cipher.getInstance(ALGORITHM_STR); // 创建密码器
        cipher.init(Cipher.ENCRYPT_MODE, key);// 初始化
        byte[] aesByes = cipher.doFinal(data.getBytes());
        String hexStr = HexByte2Util.parseByte2HexStr(aesByes);
        return Base64Utill.encode(hexStr);

    }

    /**
     * AES解密 base64->hex->aes
     * @param base64Data
     * @return
     * @throws Exception
     */
    public String decryptData(String base64Data) throws Exception{
        Cipher cipher = Cipher.getInstance(ALGORITHM_STR);
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] data = HexByte2Util.parseHexStr2Byte(Base64Utill.decode(base64Data));
        return new String(cipher.doFinal(data));
    }

    /**
     * hex字符串 转 byte数组
     * @param s
     * @return
     */
    private static byte[] hex2byte(String s) {
        if (s.length() % 2 == 0) {
            return hex2byte (s.getBytes(), 0, s.length() >> 1);
        } else {
            return hex2byte("0"+s);
        }
    }

    private static byte[] hex2byte (byte[] b, int offset, int len) {
        byte[] d = new byte[len];
        for (int i=0; i<len*2; i++) {
            int shift = i%2 == 1 ? 0 : 4;
            d[i>>1] |= Character.digit((char) b[offset+i], 16) << shift;
        }
        return d;
    }

    public static void main(String[] args) throws Exception {
        AESUtil util = new AESUtil("aWQ&c2nkKuCRX=Eg"); // 密钥
        String enStr = "Y2ZjMmIzMjVkNDBkYmQwZmNlNGJlZGM5MTgwZDg4OTg1YTQwYmRjN2QxNmJjNDIzNWZkMDMwNDIwNGQ5Mjg0NTE4MzFlZGUxZDNlMWMwODRlM2M0YmIwZjg3OTIyMjUwZTM2OTc1YjBiMTA1ODk0YmM2MGMyYmFhMWYzMGFhNzA=";
        System.out.println("cardNo:"+util.encryptData("https://openload.co/embed/AkY726WjMt0/")); // 加密
        System.out.println(util.decryptData(enStr));

    }
}
