package net.wangxj.util.file;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.UUID;
import java.util.zip.*;

import net.wangxj.util.string.UuidUtil;
import org.apache.commons.io.FilenameUtils;

/**
 * 
 * @author huoshan
 *文件工具
 */
public class FileUtil {
	
	/**
	 * 获取文件后缀
	 * @param fileName
	 * @return
	 */
	public static String getFileSuffix(String fileName){
		return fileName.substring(fileName.lastIndexOf("."));
	}
	
	public static String generateUuidName(String fileName){
		return UuidUtil.newGUID() + getFileSuffix(fileName);
	}
	
	
	/**
	 * 生成唯一文件名
	 * @return 生成的文件名
	 */
	public static String mkFileName(String fileName){
	    return UUID.randomUUID().toString()+"_"+fileName;
	}
	
	public static String getAllPathByPathAndFileName(String filePath , String fileName){
		if(filePath.endsWith(File.separator)){
			return filePath+mkFileName(fileName);
		}
		else{
			return filePath+File.separator+mkFileName(fileName);
		}
	}
	
	/**
	 * 生成存储文件子路径
	 * @param savePath
	 * @param fileName
	 * @return
	 */
	public static String mkFilePath(String savePath,String fileName){
	    //得到文件名的hashCode的值，得到的就是filename这个字符串对象在内存中的地址
	    int hashcode = fileName.hashCode();
	    int dir1 = hashcode&0xf;
	    int dir2 = (hashcode&0xf0)>>4;
        String dir; 
	    //构造新的保存目录
        if(savePath.endsWith(File.separator)){
          dir = savePath+dir1+File.separator+dir2+File.separator;
        }
        else{
        	dir = savePath +File.separator + dir1 + File.separator + dir2+File.separator;
        }
	    //File既可以代表文件也可以代表目录
	    File file = new File(dir);
	    if(!file.exists()){
	        file.mkdirs();
	    }
	    return dir;
	}
	
	/**
	 * 根据路径和原文件名生成唯一文件路径全名
	 * @param savePath
	 * @param fileName
	 * @return
	 */
	public static String generteUuidFileAllPath(String savePath,String fileName){
		String filePath = mkFilePath(savePath, fileName);
		if(filePath.endsWith(File.separator)){
			return filePath+mkFileName(fileName);
		}
		else{
			return filePath+File.separator+mkFileName(fileName);
		}
	}
	
	/**
	 * 根据路径全名获取文件名
	 * @param pathUrl
	 * @return
	 */
	public static String getFileNameByPathUrl(String pathUrl){
		return pathUrl.substring(pathUrl.lastIndexOf(File.separator)+1);
	}
	
	/**
	 * 根据路径全名获取文件后缀
	 * @param pathUrl
	 * @return
	 */
	public static String getSuffixByPathUrl(String pathUrl){
		return getFileSuffix(getFileNameByPathUrl(pathUrl));
	}
	
	static final int BUFFER = 8192;
	
	
	/**
	 * 压缩文件或文件夹
	 * @param srcPath
	 * @param dstPath
	 * @throws IOException
	 */
	public static void compress(String srcPath , String dstPath) throws IOException{
	    File srcFile = new File(srcPath);
	    File dstFile = new File(dstPath);
	    if (!srcFile.exists()) {
	        throw new FileNotFoundException(srcPath + "不存在！");
	    }

	    FileOutputStream out = null;
	    ZipOutputStream zipOut = null;
	    try {
	        out = new FileOutputStream(dstFile);
	        CheckedOutputStream cos = new CheckedOutputStream(out,new CRC32());
	        zipOut = new ZipOutputStream(cos);
	        String baseDir = "";
	        compress(srcFile, zipOut, baseDir);
	    }
	    finally {
	        if(null != zipOut){
	            zipOut.close();
	            out = null;
	        }

	        if(null != out){
	            out.close();
	        }
	    }
	}

	private static void compress(File file, ZipOutputStream zipOut, String baseDir) throws IOException{
	    if (file.isDirectory()) {
	        compressDirectory(file, zipOut, baseDir);
	    } else {
	        compressFile(file, zipOut, baseDir);
	    }
	}

	/** 压缩一个目录 */
	private static void compressDirectory(File dir, ZipOutputStream zipOut, String baseDir) throws IOException{
	    File[] files = dir.listFiles();
	    for (int i = 0; i < files.length; i++) {
	        compress(files[i], zipOut, baseDir + dir.getName() + "/");
	    }
	}

	/** 压缩一个文件 */
	private static void compressFile(File file, ZipOutputStream zipOut, String baseDir)  throws IOException{
	    if (!file.exists()){
	        return;
	    }

	    BufferedInputStream bis = null;
	    try {
	        bis = new BufferedInputStream(new FileInputStream(file));
	        ZipEntry entry = new ZipEntry(baseDir + file.getName());
	        zipOut.putNextEntry(entry);
	        int count;
	        byte data[] = new byte[BUFFER];
	        while ((count = bis.read(data, 0, BUFFER)) != -1) {
	            zipOut.write(data, 0, count);
	        }

	    }finally {
	        if(null != bis){
	            bis.close();
	        }
	    }
	}
	
	
	/**
     * 递归删除目录下的所有文件及子目录下所有文件
     * @param dir 将要删除的文件目录
     * @return boolean Returns "true" if all deletions were successful.
     *                 If a deletion fails, the method stops attempting to
     *                 delete and returns "false".
     */
    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
        	//递归删除目录中的子目录下
	        for (int i=0; i<children.length; i++) {
	            boolean success = deleteDir(new File(dir, children[i]));
	            if (!success) {
	                return false;
	            }
	        }
        }
        // 目录此时为空，可以删除
        return dir.delete();
    }


	/**
	 * 替换文件中字符串
	 * @param srcStr
	 * @param replaceStr
	 * @throws IOException
	 */
	public static void updateFile(String inputFile, String srcStr, String replaceStr) throws IOException {
		// 读
		File file = new File(inputFile);
		FileReader in = new FileReader(file);
		BufferedReader bufIn = new BufferedReader(in);
		// 内存流, 作为临时流
		CharArrayWriter  tempStream = new CharArrayWriter();
		// 替换
		String line = null;
		while ( (line = bufIn.readLine()) != null) {
			// 替换每行中, 符合条件的字符串
			line = line.replaceAll(srcStr, replaceStr);
			// 将该行写入内存
			tempStream.write(line);
			// 添加换行符
			tempStream.append(System.getProperty("line.separator"));
		}
		// 关闭 输入流
		bufIn.close();
		// 将内存中的流 写入 文件
		FileWriter out = new FileWriter(file);
		tempStream.writeTo(out);
		out.close();
	}

	/**
	 * 文件大小以 M 为单位
	 * @param file
	 * @return
	 */
	public static int getFileSize(String file){
		long length = new File(file).length();
		double mb = length / (double) (1000 * 1000);
		return new Integer((int) Math.round(mb));
	}

	/**
	 * 文件大小以 Kb 为单位
	 * @param file
	 * @return
	 */
	public static int getKbFileSize(String file){
		long length = new File(file).length();
		double kb = length / (double) (1000);
		return new Integer((int) Math.round(kb));
	}

	/**
	 * 创建文件夹如果不存在
	 * @param filename
	 */
	public static void makeFileNoExist(String filename){
		if(!new File(filename).exists()){
			new File(filename).mkdir();
		}
	}


	/**
	 * 解压到指定目录
	 * @param zipPath
	 * @param descDir
	 */
	public static void unZipFiles2(String zipPath, String descDir) throws IOException {
		unZipFiles(new File(zipPath), descDir);
	}

	/**
	 * 解压文件到指定目录
	 * 解压后的文件名，和之前一致
	 * @param zipFile	待解压的zip文件
	 * @param descDir 	指定目录
	 */
	@SuppressWarnings("rawtypes")
	public static void unZipFiles(File zipFile, String descDir) throws IOException {

		ZipFile zip = new ZipFile(zipFile, Charset.forName("GBK"));//解决中文文件夹乱码

		for (Enumeration<? extends ZipEntry> entries = zip.entries(); entries.hasMoreElements(); ) {
			ZipEntry entry = (ZipEntry) entries.nextElement();
			String zipEntryName = entry.getName();
			InputStream in = zip.getInputStream(entry);
			String outPath = FilenameUtils.concat(descDir, zipEntryName).replaceAll("\\*", "/");

			// 判断路径是否存在,不存在则创建文件路径
			File file = new File(outPath.substring(0, outPath.lastIndexOf('/')));
			if (!file.exists()) {
				file.mkdirs();
			}
			// 判断文件全路径是否为文件夹,如果是上面已经上传,不需要解压
			if (new File(outPath).isDirectory()) {
				continue;
			}
			// 输出文件路径信息
//			System.out.println(outPath);

			FileOutputStream out = new FileOutputStream(outPath);
			byte[] buf1 = new byte[1024];
			int len;
			while ((len = in.read(buf1)) > 0) {
				out.write(buf1, 0, len);
			}
			in.close();
			out.close();
		}
		System.out.println("******************解压完毕********************");
	}

	public static void copyFileUsingFileStreams(String src, String dest)throws IOException{
		copyFileUsingFileStreams(new File(src), new File(dest));
	}

	public static void copyFileUsingFileStreams(File src, File dest)throws IOException{
		InputStream input = new FileInputStream(src);
		copyFileUsingFileStreams(input, dest);
	}

	/**
	 * 拷贝文件
	 * @param input
	 * @param dest
	 * @throws IOException
	 */
	public static void copyFileUsingFileStreams(InputStream input, File dest)
			throws IOException {
		OutputStream output = null;
		try {
			output = new FileOutputStream(dest);
			byte[] buf = new byte[1024];
			int bytesRead;
			while ((bytesRead = input.read(buf)) > 0) {
				output.write(buf, 0, bytesRead);
			}
		} finally {
			input.close();
			output.close();
		}
	}

	/**
	 * 下载文件
	 * @param urlString
	 * @param filename
	 * @param savePath
	 * @throws Exception
	 */
	public static void download(String urlString, String filename,String savePath) throws Exception {
		// 构造URL
		URL url = new URL(urlString);
		// 打开连接
		URLConnection con = url.openConnection();
		//设置请求超时为5s
		con.setConnectTimeout(5*1000);
		// 输入流
		InputStream is = con.getInputStream();

		// 1K的数据缓冲
		byte[] bs = new byte[1024];
		// 读取到的数据长度
		int len;
		// 输出的文件流
		File sf=new File(savePath);
		if(!sf.exists()){
			sf.mkdirs();
		}
		OutputStream os = new FileOutputStream(FilenameUtils.concat(sf.getPath(), filename));
		// 开始读取
		while ((len = is.read(bs)) != -1) {
			os.write(bs, 0, len);
		}
		// 完毕，关闭所有链接
		os.close();
		is.close();
	}


	public static String readTxtFileIntoString(String filePath)
	{
		String str = "";
		try
		{
			String encoding = "GBK";
			File file = new File(filePath);
			InputStreamReader read = new InputStreamReader(
					new FileInputStream(file), encoding);// 考虑到编码格式
			BufferedReader bufferedReader = new BufferedReader(read);
			String lineTxt = null;

			while ((lineTxt = bufferedReader.readLine()) != null)
			{
				str += lineTxt;
			}
			bufferedReader.close();
			read.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return str;
	}


}
