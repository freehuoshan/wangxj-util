package net.wangxj.util.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.digester.rss.Item;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.ProgressListener;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;

/**
 * 文件上传工具类
 * @author huoshan
 *
 */
public class HuoshanFileUploadUtil {

		private Logger log = LoggerFactory.getLogger(HuoshanFileUploadUtil.class);
		
		private String destDir;
		private String tempDir;
		private Integer tempFileMaxSize;
		private Integer singleMaxSize;
		private Integer sumMaxSize;
		
		public HuoshanFileUploadUtil() {
			super();
		}

		/**
		 * 
		 * @param destDir 			上传目标路径
		 * @param tempDir			存放缓存路径
		 * @param tempFileMaxSize 	缓存大小
		 * @param singleMaxSize  	允许上传最大单个文件
		 * @param sumMaxSize		总共允许上传大小
		 */
		public HuoshanFileUploadUtil(String destDir, String tempDir, Integer tempFileMaxSize, Integer singleMaxSize,
				Integer sumMaxSize) {
			super();
			this.destDir = destDir;
			this.tempDir = tempDir;
			this.tempFileMaxSize = tempFileMaxSize;
			this.singleMaxSize = singleMaxSize;
			this.sumMaxSize = sumMaxSize;
		}
		
		/**
		 * 上传文件
		 * @param request
		 * @param response
		 * @return
		 * @throws Exception 
		 */
		public String uploadFileToDirectory(HttpServletRequest request, HttpServletResponse response) throws Exception{
			
			if(!this.validate(request)){
				return "上传失败,请重试";
			}
            //1、创建一个DiskFileItemFactory工厂
            DiskFileItemFactory diskFileItemFactory = new DiskFileItemFactory();
            //设置工厂的缓冲区的大小，当上传的文件大小超过缓冲区的大小时，就会生成一个临时文件存放到指定的临时目录当中。
            diskFileItemFactory.setSizeThreshold(this.tempFileMaxSize);
            //设置上传时生成的临时文件的保存目录
            diskFileItemFactory.setRepository(new File(tempDir));
            //2、创建一个文件上传解析器
            ServletFileUpload fileUpload = new ServletFileUpload(diskFileItemFactory);
            //设置上传单个文件的大小的最大值，目前是设置为1024*1024字节，也就是1MB
            fileUpload.setFileSizeMax(singleMaxSize);
            //设置上传文件总量的最大值，最大值=同时上传的多个文件的大小的最大值的和，目前设置为10MB
            fileUpload.setSizeMax(sumMaxSize);
            //解决上传文件名的中文乱码
            fileUpload.setHeaderEncoding("UTF-8");
            //监听文件上传进度
            fileUpload.setProgressListener(new ProgressListener(){
                public void update(long pBytesRead, long pContentLength, int arg2) {
                	System.out.println("------"+arg2+"------------");
                    HttpSession session = request.getSession(true);
                    String byteProcess = pBytesRead+"/"+pContentLength+"byte";
                    String rate = Math.round(new Float(pBytesRead)/new Float(pContentLength)*100) + "%";
                    session.setAttribute("byteProcess", byteProcess);
                    session.setAttribute("rate", rate);
                }
            });
            //4、使用ServletFileUpload解析器解析上传数据，解析结果返回的是一个List<FileItem>集合，每一个FileItem对应一个Form表单的输入项
            List<FileItem> list = fileUpload.parseRequest(request);
            List<Map<String, Object>> responseList = new ArrayList<>();
            for (FileItem item : list) {
                //如果item中封装的是普通输入项的数据
                if(item.isFormField()){
                	log.debug("属性"+item.getFieldName()+"为普通字段，不做解析");
                	continue;
                }else{
                	//如果item中封装的是上传文件，得到上传的文件名称，
                    String fileName = item.getName();
                    if(fileName==null||fileName.trim().equals("")){
                    	log.debug("该文件名为空，不做解析，跳过解析下一个");
                        continue;
                    }
                    responseList.add(this.parseFileItemToDirectory(item, request));
                }
                
            }
           
            return JSON.toJSONString(responseList);
		}
		
		/**
		 * 验证
		 * @param request
		 */
		public Boolean validate(HttpServletRequest request){
			
			File destFile = new File(this.destDir);
			File tempFile = new File(this.tempDir);
			if(destFile.isFile() || tempFile.isFile()){
				String whichDIr = destFile.isFile() ? "destFile" : "tempFile";
				log.error("所指定"+whichDIr+"不是路径，执行终止！！！");
				return false;
			}
			else if(!ServletFileUpload.isMultipartContent(request)){
				log.debug("该请求不是上传表单，执行终止！！！");
	            return false;
	        }
			else if(!destFile.isDirectory()){
	            log.debug("所指定的路径不存在，将开始创建....");
	            destFile.mkdir();
	            log.debug("路径"+destDir+"创建完成");
	        }
			return true;
		}
			
			
		/**
		 * 解析request file上传到指定路径
		 * @param fileItem
		 * @param request
		 * @return
		 * @throws Exception 
		 */
		public Map<String, Object> parseFileItemToDirectory(FileItem fileItem,HttpServletRequest request) throws Exception{
			
			String fileName = fileItem.getName();
			String fileAllPath = FileUtil.getAllPathByPathAndFileName(destDir,fileName);
			fileItem.write(new File(fileAllPath));
			log.debug("文件:--"+fileName+"--上传成功");
			
			Map<String, Object> responseMap = new HashMap<>();
			responseMap.put("url", fileAllPath);
			responseMap.put("thumbnail_url","");
			responseMap.put("name", fileName);
			responseMap.put("type", fileItem.getContentType());
			responseMap.put("size", fileItem.getSize());
			responseMap.put("delete_url", "http://url.to/delete /file/");
			responseMap.put("delete_type", "DELETE");
	        fileItem.delete();
	        return responseMap;
		}
		
		/**
		 * 获取文件上传进度
		 * @param request
		 * @return
		 */
		public String getProcessValue(HttpServletRequest request){
			HttpSession session = request.getSession();
			String byteProcess = (String) session.getAttribute("byteProcess");
			String rate = (String)session.getAttribute("rate");
			Map<String, String> processMap = new HashMap<>();
			processMap.put("byteProcess", byteProcess);
			processMap.put("rate", rate);
			
			return JSON.toJSONString(processMap);
		}

}
