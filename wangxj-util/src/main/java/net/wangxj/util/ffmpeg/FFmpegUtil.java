package net.wangxj.util.ffmpeg;

import net.wangxj.util.string.StringUtil;
import net.wangxj.util.string.UuidUtil;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FFmpegUtil {

    public static Logger log = Logger.getLogger(FFmpegUtil.class);

    public static String[] ffmpegFormats = {"asx", "asf", "mpg", "wmv", "3gp", "mp4", "mov", "avi", "flv"};
    public static String[] noFformats = {"wmv9", "rm", "rmvb"};

    /**
     * 该文件是否可被 ffmpeg　处理
     * @param extraName 后缀名,不包含.
     * @return true　可以, false 不可以
     */
    public static boolean checkContentType(String extraName) {
        String type = extraName.toLowerCase();
        if(Arrays.asList(ffmpegFormats).contains(type)){
            return true;
        }
        return false;
    }

    /**
     * 获取视频总时间 分钟
     * @param videoPath    视频路径
     * @return
     */
    public static int getVideoTime(String videoPath) {
        List<String> commands = new java.util.ArrayList<String>();
        commands.add("ffmpeg");
        commands.add("-i");
        commands.add(videoPath);
        try {
            ProcessBuilder builder = new ProcessBuilder();
            builder.command(commands);
            final Process p = builder.start();

            //从输入流中读取视频信息
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();

            //从视频信息中解析时长
            String regexDuration = "Duration: (.*?), start: (.*?), bitrate: (\\d*) kb\\/s";
            Pattern pattern = Pattern.compile(regexDuration);
            Matcher m = pattern.matcher(sb.toString());
            if (m.find()) {
                int time = getTimelen(m.group(1));
                return time;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    //格式:"00:00:10.68"
    private static int getTimelen(String timelen){
        int min=0;
        String strs[] = timelen.split(":");
        if (strs[0].compareTo("0") > 0) {
            min+=Integer.valueOf(strs[0])*60*60;//秒
        }
        if(strs[1].compareTo("0")>0){
            min+=Integer.valueOf(strs[1])*60;
        }
        if(strs[2].compareTo("0")>0){
            min+=Math.round(Float.valueOf(strs[2]));
        }
        return min/60;
    }


    /**
     * mp4转ts
     * @param inputFile
     * @param outputFile
     */
    public static void coverMp4ToTs(String inputFile, String outputFile) throws IOException {
        //mp4 轉 ts ffmpeg -y -i 369.mp4 -vcodec copy -acodec copy -vbsf h264_mp4toannexb abc.ts
        // 构建命令
        List<String> command = new ArrayList();
        command.add("ffmpeg");
        command.add("-y");
        command.add("-i");
        command.add(inputFile);
        command.add("-vcodec");
        command.add("copy");
        command.add("-acodec");
        command.add("copy");
        command.add("-vbsf");
        command.add("h264_mp4toannexb");
        command.add(outputFile);

        execute(command);
    }

    /**
     * ts 生成碎片ts 和 m3u8
     * @param inputFile
     * @param outputM8File
     * @param stsRegex
     * @param times 切片长度 s/秒
     */
    public static void convertTsToM3u8(String inputFile, String outputM8File, String stsRegex, Integer times) throws IOException {

        //ts 賺 m3u8 ffmpeg -i abc.ts -c copy -map 0 -f segment -segment_list playlist.m3u8 -segment_time 5 abc%03d.ts
        List<String> command = new ArrayList();
        command.add("ffmpeg");
        command.add("-i");
        command.add(inputFile);
        command.add("-c");
        command.add("copy");
        command.add("-map");
        command.add("0");
        command.add("-f");
        command.add("segment");
        command.add("-segment_list");
        command.add(outputM8File);
        command.add("-segment_time");
        command.add(String.valueOf(times));
        command.add(stsRegex);

        execute(command);
    }

    /**
     * 生成預覽視頻
     * @param inputFile
     * @param outputFile
     * @param wh 預覽視頻長:款
     * @param snipp  幾個片段 每個片段時長
     * @throws IOException
     */
    public static void generatePreviewVideo(String inputFile, String outputFile, String wh, Integer ... snipp) throws IOException {
        //复制 shell 到 /tmp
        InputStream in = FFmpegUtil.class.getResourceAsStream("/video_preview.sh");
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String tempName = FilenameUtils.concat("/tmp/", UuidUtil.newGUID().concat(".sh"));
        File tempFile = new File(tempName);
        Writer writer = new FileWriter(tempFile, true);
        String line;
        while ((line = reader.readLine()) != null){
            writer.write(line);
            writer.write(System.getProperty("line.separator"));// 添加换行符
        }
        reader.close();
        writer.close();
        //授权脚本
        List<String> command = new ArrayList();
        command.add("chmod");
        command.add("777");
        command.add(tempName);
        execute(command);
        command.clear();
        //执行脚本
        command.add(tempName);
        command.add(inputFile);
        command.add(outputFile);
        if(StringUtil.isBlank(wh)){
            command.add(wh);
        }
        for(Integer param : snipp){
            command.add(String.valueOf(param));
        }
        execute(command);
        tempFile.deleteOnExit();
    }

    /**
     * 生成视频缩略图
     * @param inputFile
     * @param outputImgRegex
     * @param timeStr 截图间隔时长 1/60 一分 1/600 十分
     */
    public static void generatePreviewImg(String inputFile, String outputImgRegex, String timeStr) throws IOException {
        //ffmpeg -i 369.mp4 -vf fps=1/600 out%d.jpg
        List<String> command = new ArrayList<>();
        command.add("ffmpeg");
        command.add("-i");
        command.add(inputFile);
        command.add("-vf");
        command.add("fps=".concat(timeStr));
        command.add(outputImgRegex);
        execute(command);

    }



    public static void execute(List<String> command) throws IOException {
        // 执行操作
        ProcessBuilder builder = new ProcessBuilder(command);
        Process process = builder.start();
        InputStream errorStream = process.getErrorStream();
        InputStreamReader isr = new InputStreamReader(errorStream);
        BufferedReader br = new BufferedReader(isr);
        String line;
        while ((line = br.readLine()) != null) {
           System.out.println("日志:" + line);
        }
        if (br != null) {
            br.close();
        }
        if (isr != null) {
            isr.close();
        }
        if (errorStream != null) {
            errorStream.close();
        }

    }

}
