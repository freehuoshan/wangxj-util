package net.wangxj.util.constant;

public class RegexConstant {
	
	/**
	 * 以大小写字母与下划线混合字符串，下划线位于中间
	 */
	public static final String LETTER_UNDERLINE = "^(?!_)(?!.*?_$)[a-zA-Z_]+$";
	/**
	 * 以字母数字下划线组合，下划线位于中间
	 */
	public static final String LETTER_NUMBER_UNDERLINE = "^(?!_)(?!.*?_$)[a-zA-Z0-9_]+$";
	/**
	 * 字母数字下划线自由组合
	 */
	public static final String LETTER_NUMBER_UNDERLINE_FREE = "^\\w+$";
	/**
	 * 域名
	 */
	public static final String DOMAIN = "^((http://)|(https://))?([a-zA-Z0-9]([a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])?\\.)+[a-zA-Z]{2,6}";
	/**
	 * 2-25个以上汉字
	 */
	public static final String MORETHAN_TWO_CHINESECHAR= "^[\u4e00-\u9fa5]{2,25}$";
	/**
	 * 2-16个汉字
	 */
	public static final String TWO_TO_SIXTEEN_CHINESECHAR = "^[\u4e00-\u9fa5]{2,16}$";
	/**
	 * 32位uuid
	 */
	public static final String UUID_32= "^[a-z0-9]{32}$";
	/**
	 * 32位uuid后（00744193d6304fc0a38dc299eae382d1-1）
	 */
	public static final String UUID_32_1_2 = "^([a-z0-9]{32}-[1-2])*";
	/**
	 * 邮箱
	 */
	public static final String EMAIL = "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
	/**
	 * 手机电话
	 */
	public static final String PHONE = "(\\(\\d{3,4}\\)|\\d{3,4}-|\\s)?\\d{7,14}";
	/**
	 * 资源链接（以或不以斜杠开头或结尾,字母下划线数字斜杠自由组合）
	 */
	public static final String RESOURCE_HREF = "(^/)?([\\w/]*)(/)?$";
	/**
	 * 排序字符串
	 */
	public static final String ORDER_STR = "desc|asc";

}
