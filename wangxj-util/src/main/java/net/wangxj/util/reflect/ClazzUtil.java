package net.wangxj.util.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author huoshan
 * created by 2017年5月28日 下午1:38:17
 * 
 */
public class ClazzUtil {
	
	public static boolean isEmptyofObj(Object obj) throws IllegalArgumentException, IllegalAccessException{
		
		for(Field field : obj.getClass().getDeclaredFields()){
			field.setAccessible(true);
			if(field.getName().equals("serialVersionUID")){
				continue;
			}
			Object fieldValue = field.get(obj);
			if( fieldValue != null &&  !"".equals(fieldValue)){
				return false;
			}
		}
		return true;
	}
	
	/** 
	    * java反射bean的get方法 
	    *  
	    * @param objectClass 
	    * @param fieldName 
	    * @return 
	    */  
	   @SuppressWarnings("unchecked")  
	   public static Method getGetMethod(Class objectClass, String fieldName) {  
	       StringBuffer sb = new StringBuffer();  
	       sb.append("get");  
	       sb.append(fieldName.substring(0, 1).toUpperCase());  
	       sb.append(fieldName.substring(1));  
	       try {  
	           return objectClass.getMethod(sb.toString());  
	       } catch (Exception e) {  
	       }  
	       return null;  
	   }  
	
	   /** 
	     * java反射bean的set方法 
	     *  
	     * @param objectClass 
	     * @param fieldName 
	     * @return 
	     */  
	    @SuppressWarnings("unchecked")  
	    public static Method getSetMethod(Class objectClass, String fieldName) {  
	        try {  
	            Class[] parameterTypes = new Class[1];  
	            Field field = objectClass.getDeclaredField(fieldName);  
	            parameterTypes[0] = field.getType();  
	            StringBuffer sb = new StringBuffer();  
	            sb.append("set");  
	            sb.append(fieldName.substring(0, 1).toUpperCase());  
	            sb.append(fieldName.substring(1));  
	            Method method = objectClass.getMethod(sb.toString(), parameterTypes);  
	            return method;  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
	        return null;  
	    }  

}
