package net.wangxj.util.listener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author huoshan
 * log4J 日志动态配置工具类
 */
public class ConfigLog4jListener implements ServletContextListener{

	Logger log = LoggerFactory.getLogger(ConfigLog4jListener.class);
	
	public void contextInitialized(ServletContextEvent sce) {
		
		ServletContext servletContext = sce.getServletContext();
		String log4jConfigPropertiesPath = servletContext.getInitParameter("log4jProperties");
		String classpathStr = this.getClass().getClassLoader().getResource("").getPath();
		if(log4jConfigPropertiesPath.startsWith("classpath:")){
			log4jConfigPropertiesPath = classpathStr + log4jConfigPropertiesPath.split(":")[1];
		}
		else{
			log4jConfigPropertiesPath = classpathStr + log4jConfigPropertiesPath;
		}
		if(log4jConfigPropertiesPath == null || "".equals(log4jConfigPropertiesPath)){
			System.out.println("请配置日志配置文件路径");
			return;
		}
//		如果日志配置文件路径不为空
		if(!log4jConfigPropertiesPath.endsWith(".properties")){
			System.out.println("请配置propertiesl路径，格式错误");
			return;
		}
		InputStream inStream;
		
		try {
			inStream = new FileInputStream(log4jConfigPropertiesPath);
			Properties properties = new Properties();
			properties.load(inStream);
			Enumeration<Object> keys = properties.keys();
			while(keys.hasMoreElements()){
				String nextElement = (String)keys.nextElement();
				String value = properties.getProperty( nextElement);
				System.setProperty(nextElement.trim(), value.trim());
			}
		} catch (FileNotFoundException e) {
			System.out.println("未找到配置的日志配置文件");
			e.printStackTrace();
			return;
		} catch (IOException e) {
			System.out.println("加载日志配置文件出错");
			return;
		}
		
		
	}

	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		
	}
	
}
