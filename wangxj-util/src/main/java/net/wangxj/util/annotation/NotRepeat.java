package net.wangxj.util.annotation;

import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author huoshan
 * created by 2017年5月6日 上午10:26:25
 * 注解不可重复字段
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface NotRepeat {
	String message() default "字段重复";
}
