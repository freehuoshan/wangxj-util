/**
 * author: wangxj
 * create time: 上午10:07:27
 */
package net.wangxj.util.annotation.util;

import java.lang.reflect.Field;

import javax.xml.bind.annotation.XmlElement;

import net.wangxj.util.validate.ValidationResult;

/**
 * @author huoshan
 * created by 2017年5月8日 上午10:07:27
 * 
 */
public class XmlElementUtil {
	
	/**
	 * 该class中的所有注解＠XMlelement　name值是否存在nameStr
	 * @return 存在返回null , 不存在返回错误信息
	 */
	public static ValidationResult isExistName(Class<? extends Object> clazz, String nameStr){
		
		Field[] declaredFields = clazz.getDeclaredFields();
		for (Field field : declaredFields) {
			XmlElement xmlAnnotation = field.getAnnotation(XmlElement.class);
			if(xmlAnnotation == null){
				continue;
			}
			String name = xmlAnnotation.name();
			if(name.equals(nameStr)){
				return null;
			}
		}
		ValidationResult validateRes = new ValidationResult();
		validateRes.setErrorMsg("不存在" + nameStr);
		return validateRes;
		
	}

}
