package net.wangxj.util.annotation.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.wangxj.util.annotation.NotRepeat;
import net.wangxj.util.string.StringUtil;

/**
 * @author huoshan
 * created by 2017年5月6日 上午10:37:30
 * 
 */
public class NotRepeatUtil {
	
	/**
	 * build obj depend on annotated field;
	 * @param orgObj
	 * @return
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws ClassNotFoundException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	public static List<Map<String,Object>> buildObjFromAnnotatedField(final Object orgObj) throws NoSuchMethodException, SecurityException, ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException{
		Class clazz = orgObj.getClass();
		List<Field> annotatedFields = AnnotationUtil.getAnnotatedFields(orgObj.getClass(), NotRepeat.class);
		//{{"fieldName" : object}....}
		List<Map<String,Object>> fieldObjectList = new ArrayList<>();
		for (Field field : annotatedFields) {
			String name = field.getName();
			Class<?> type = field.getType();
			Object newObj = clazz.newInstance();
			Method setMethod = clazz.getMethod(StringUtil.getSetMethodNameByFieldName(name), type);
			Method getMethod = clazz.getMethod(StringUtil.getGetMethodNameByFieldName(name),null);
			Object value = getMethod.invoke(orgObj,null);
			setMethod.invoke(newObj, value);
			//{"fieldName" : object}
			Map<String,Object> filedObjectMap = new HashMap<>();
			filedObjectMap.put(name, newObj);
			fieldObjectList.add(filedObjectMap);
		}
		return fieldObjectList;
	}
	

}
