/**
 * author: wangxj
 * create time: 上午10:51:50
 */
package net.wangxj.util.annotation.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author huoshan
 * created by 2017年5月6日 上午10:51:50
 * 
 */
public class AnnotationUtil {

	/**
	 * get all fields annotated by annotation in the clazz class
	 * @param clazz
	 * @param annotation
	 * @return
	 */
	public static List<Field> getAnnotatedFields(final Class< ? extends Object> clazz , final Class annotation){
		List<Field> annotatedFieldList = new ArrayList<>();
		Field[] allFields = clazz.getDeclaredFields();
		for (Field field : allFields) {
			Annotation annotatedField = field.getAnnotation(annotation);
			if(annotatedField != null){
				annotatedFieldList.add(field);
			}
		}
		return annotatedFieldList;
	}
}
