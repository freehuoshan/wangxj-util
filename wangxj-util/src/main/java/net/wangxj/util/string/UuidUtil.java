package net.wangxj.util.string;

import java.util.UUID;

public class UuidUtil{
	
	public static String newGUID() 
	{ 
		UUID uuid = UUID.randomUUID(); 
		return uuid.toString().replace("-", ""); 
	} 
}
