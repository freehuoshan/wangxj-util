package net.wangxj.util.string;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import net.wangxj.util.number.NumberUtil;

public class TimeUtil {
	
	/**
	 * 现在时间字符串格式为yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String getNowStr(){
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	}
	
	public static String getNowDay(){
		return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	}
	
	/**
	 * 为 yyy-MM-dd HH:mm:ss 格式日期添加时间
	 * @param source
	 * @param year
	 * @param month
	 * @param day
	 * @return
	 * @throws ParseException
	 */
	public static String addTime(String source, Integer year, Integer month, Integer day, Integer hours) throws ParseException{
		DateFormat fmt =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = fmt.parse(source);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		if(StringUtil.isNotBlank(year)){
			calendar.add(Calendar.YEAR, year);
		}
		if(StringUtil.isNotBlank(month)){
			calendar.add(Calendar.MONTH, month);
		}
		if(StringUtil.isNotBlank(day)){
			calendar.add(Calendar.DAY_OF_YEAR, day);
		}
		if(StringUtil.isNotBlank(hours)){
			calendar.add(Calendar.HOUR_OF_DAY, hours);
		}
		date = calendar.getTime();
		return fmt.format(date);
	}
	
	/**
	 * 获取随机时间 时分秒
	 * @return
	 */
	public static String randomTime(){
		String hour = NumberUtil.getRandom(1, 23);
		String minute = NumberUtil.getRandom(0, 59);
		String second = NumberUtil.getRandom(0, 59);
		return NumberUtil.setLengthNumber(hour, 2).concat(":")
				.concat(NumberUtil.setLengthNumber(minute, 2)).concat(":")
				.concat(NumberUtil.setLengthNumber(second, 2));
	}
	
	/**
	  * 获取两个时间的时间差，精确到毫秒
	  * @param str
	  * @return
	  */
	 public static String TimeDifference(long start, long end) {
		  long between = end - start;
		  long day = between / (24 * 60 * 60 * 1000);
		  long hour = (between / (60 * 60 * 1000) - day * 24);
		  long min = ((between / (60 * 1000)) - day * 24 * 60 - hour * 60);
		  long s = (between / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
		  long ms = (between - day * 24 * 60 * 60 * 1000 - hour * 60 * 60 * 1000
		    - min * 60 * 1000 - s * 1000);
		  String timeDifference = day + "天" + hour + "小时" + min + "分" + s + "秒" + ms
		    + "毫秒";
		  return timeDifference;
	 }
}
