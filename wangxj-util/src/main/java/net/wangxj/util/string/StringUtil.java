package net.wangxj.util.string;

import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.StringUtils;


public class StringUtil extends StringUtils{
	
	/**
	 * 获取驼峰反向字符串
	 * @param source
	 * @return
	 */
	public static String getNumpReverse(String source){
		
		if(StringUtil.isBlankOrNull(source)){
			return "";
		}
		String regexStr = "[A-Z]";
		Matcher matcher = Pattern.compile(regexStr).matcher(source);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			String g = matcher.group();
			matcher.appendReplacement(sb, "_" + g.toLowerCase());
		}
		matcher.appendTail(sb);
		if (sb.charAt(0) == '_') {
			sb.delete(0, 1);
		}
		return sb.toString();
	}
	
	/**
	 * 字符串是否为null或空字符串
	 * @param source
	 * @return
	 */
	public static boolean isBlankOrNull(String source){
		return (source == null || "".equals(source.trim()));
	}
	
	/**
	 * get set method name by field name
	 * @param fieldName
	 * @return
	 */
	public static String getSetMethodNameByFieldName(String fieldName){
		
		return "set" + fieldName.substring(0,1).toUpperCase()+ fieldName.substring(1);
		
	}
	
	/**
	 * get get method name by field name
	 * @param fieldName
	 * @return
	 */
	public static String getGetMethodNameByFieldName(String fieldName){
		return "get" + fieldName.substring(0,1).toUpperCase()+ fieldName.substring(1);
	}
	
	/**
	 * obj数组转化为string数组
	 * @param objArr
	 * @return
	 */
	public static String[] objArrToStr(Object[] objArr){
		String[] strArr = new String[objArr.length];
		for (int i = 0 ; i<objArr.length ; i++) {
			strArr[i] = String.valueOf(objArr[i]);
		}
		return strArr;
	}
	
	/**
	 * 一次性判断多个或单个对象为空。
	 * 
	 * @param objects
	 * @author ztt
	 * @return 只要有一个元素为Blank，则返回true
	 */
	public static boolean isBlank(Object... objects) {
		Boolean result = false;
		for (Object object : objects) {
			if (null == object || "".equals(object.toString().trim()) || "null".equals(object.toString().trim())) {
				result = true;
				break;
			}
		}
		return result;
	}

	/**
	 * 一次性判断多个或单个对象不为空。
	 * 
	 * @param objects
	 * @author ztt
	 * @return 只要有一个元素不为Blank，则返回true
	 */
	public static boolean isNotBlank(Object... objects) {
		return !isBlank(objects);
	}

	public static boolean isBlank(String... objects) {
		Object[] object = objects;
		return isBlank(object);
	}

	public static boolean isNotBlank(String... objects) {
		Object[] object = objects;
		return !isBlank(object);
	}

	public static boolean isBlank(String str) {
		Object object = str;
		return isBlank(object);
	}

	public static boolean isNotBlank(String str) {
		Object object = str;
		return !isBlank(object);
	}
	
	public static String getTwoDecimal(Float input){
		DecimalFormat decimalFormat=new DecimalFormat(".00");//构造方法的字符格式这里如果小数不足2位,会以0补足.
		return decimalFormat.format(input);
	}

	/**
	 * 鏈接路徑地址
	 * @param pre
	 * @param suf
	 * @return
	 */
	public static String pathContact(String pre, String suf){
		if(pre.endsWith("/")){
			if(suf.startsWith("/")){
				return pre.concat(suf.substring(1));
			}
			return pre.concat(suf);
		}else{
			if(suf.startsWith("/")){
				return pre.concat(suf);
			}
			return pre.concat("/").concat(suf);
		}

	}

}
