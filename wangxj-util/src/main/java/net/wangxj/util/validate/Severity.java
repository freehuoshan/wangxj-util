package net.wangxj.util.validate;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Payload;

public class Severity {
	public static interface ISNULL extends Payload{};
	public static interface FIELDNULL extends Payload{};
	public static interface REGEXERROR extends Payload{};
	public static interface RANGEERROR extends Payload{};
	
	public static Map<String, String> errInfoMap = new HashMap<>();
	
	static{
		errInfoMap.put(ISNULL.class.getSimpleName(), "传入的值不可为空");
		errInfoMap.put(FIELDNULL.class.getSimpleName(), "某些不允许为空值的字段为空值");
		errInfoMap.put(REGEXERROR.class.getSimpleName(), "某些字段不符合规定的格式");
		errInfoMap.put(RANGEERROR.class.getSimpleName(), "某些字段超出了限制的范围");
	}
	
}
