package net.wangxj.util.validate;

import java.util.Map;
import java.util.Set;

import javax.validation.Payload;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;

/**
 * 校验结果
 * @author huoshan
 *
 */
@JSONType
public class ValidationResult {

	//校验结果是否通过
	@JSONField(name = "is_pass")
	private boolean isPass = false;
	@JSONField(name = "error_message")
	private String errorMsg = "校验错误";
	
	public ValidationResult() {
		super();
	}

	public ValidationResult(boolean isPass, String errorMsg) {
		super();
		this.isPass = isPass;
		this.errorMsg = errorMsg;
	}

	public boolean getIsPass() {
		return isPass;
	}

	public void setIsPass(boolean isPass) {
		this.isPass = isPass;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	@Override
	public String toString() {
		return "ValidationResult [isPass=" + isPass + ", errorMsg=" + errorMsg + "]";
	}
	
}
