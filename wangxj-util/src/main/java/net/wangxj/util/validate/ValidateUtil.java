package net.wangxj.util.validate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Payload;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;

/**
 * 
 * @author huoshan
 *
 */
public class ValidateUtil {

	private static Validator validator =  Validation.buildDefaultValidatorFactory().getValidator();
	
	/**
	 * 校验实体
	 * @param <F>
	 * @param obj
	 * @return
	 */
	public static <T, F> ValidationResult validateEntity(T obj,Class<F> clazz){
		 ValidationResult result = new ValidationResult();
		 
		 Set<ConstraintViolation<T>> set = validator.validate(obj, clazz);
		 if(set != null && set.size()>0){
			 List<String> errorMsgList = new ArrayList<>();
			 for(ConstraintViolation<T> cv : set){
				errorMsgList.add(cv.getMessage());
			 }
			 result.setErrorMsg(errorMsgList.get(0));
		 }
		 else{
			 result.setIsPass(true);
			 result.setErrorMsg("校验通过");
		 }
		 return result;
	}
	
	/**
	 * 校验属性
	 * @param obj
	 * @param propertyName
	 * @return
	 */
	public static <T> ValidationResult validateProperty(T obj,String propertyName){
		ValidationResult result = new ValidationResult();
		 Set<ConstraintViolation<T>> set = validator.validateProperty(obj,propertyName,Default.class);
		 if( set != null && set.size()>0){
			 List<String> errorMsgList = new ArrayList<>();
			 for(ConstraintViolation<T> cv : set){
				errorMsgList.add(cv.getMessage());
			 }
			 result.setErrorMsg(errorMsgList.get(0));
		 }
		 else{
			 result.setIsPass(true);
			 result.setErrorMsg("校验通过");
		 }
		 return result;
	}
	
}

