package net.wangxj.util.html;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.wangxj.util.string.UuidUtil;

/**
 * @author huoshan
 * created by 2017年7月4日 下午4:49:24
 * 
 */
public class HTMLUtil {
	
	
	/**  
     * @param str :  
     *            source string  
     * @param width :  
     *            string's byte width  
     * @param ellipsis :  
     *            a string added to abbreviate string bottom  
     * @return  String Object  
     *   
     */  
    public static String abbreviate(String str, int width, String ellipsis) {   
        if (str == null || "".equals(str)) {   
            return "";   
       }   
  
        int d = 0; // byte length   
        int n = 0; // char length   
        for (; n < str.length(); n++) {   
            d = (int) str.charAt(n) > 256 ? d + 2 : d + 1;   
            if (d > width) {   
                break;   
            }   
        }   
  
       if (d > width) {   
            n = n - ellipsis.length() / 2;   
            return str.substring(0, n > 0 ? n : 0) + ellipsis;   
        }   
  
       return str = str.substring(0, n);   
    }   
 
    /**  
     * 去除html标签
     * @param str :  
     *            source string  
     * @param width :  
     *            string's byte width  
     * @param ellipsis :  
     *            a string added to abbreviate string bottom  
     * @return  String Object  
    *   
     */  
   public static String Html2Text(String inputString) {   
       String htmlStr = inputString; // 含html标签的字符串   
       String textStr = "";   
       java.util.regex.Pattern p_script;   
        java.util.regex.Matcher m_script;   
        java.util.regex.Pattern p_style;   
       java.util.regex.Matcher m_style;   
        java.util.regex.Pattern p_html;   
        java.util.regex.Matcher m_html;   
  
        java.util.regex.Pattern p_html1;   
        java.util.regex.Matcher m_html1;   
  
        try {   
        	String regEx_script = "<[\\s]*?script[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?script[\\s]*?>"; // 定义script的正则表达式{或<script[^>]*?>[\\s\\S]*?<\\/script>   
            // }   
            String regEx_style = "<[\\s]*?style[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?style[\\s]*?>"; // 定义style的正则表达式{或<style[^>]*?>[\\s\\S]*?<\\/style>   
           // }   
           String regEx_html = "<[^>]+>"; // 定义HTML标签的正则表达式   
            String regEx_html1 = "<[^>]+";   
           p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);   
           m_script = p_script.matcher(htmlStr);   
            htmlStr = m_script.replaceAll(""); // 过滤script标签   
 
            p_style = Pattern.compile(regEx_style, Pattern.CASE_INSENSITIVE);   
            m_style = p_style.matcher(htmlStr);   
            htmlStr = m_style.replaceAll(""); // 过滤style标签   
  
           p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);   
            m_html = p_html.matcher(htmlStr);   
            htmlStr = m_html.replaceAll(""); // 过滤html标签   
  
            p_html1 = Pattern.compile(regEx_html1, Pattern.CASE_INSENSITIVE);   
            m_html1 = p_html1.matcher(htmlStr);   
            htmlStr = m_html1.replaceAll(""); // 过滤html标签   
  
           textStr = htmlStr;   
  
        } catch (Exception e) {   
            System.err.println("Html2Text: " + e.getMessage());   
        }   
 
       return textStr;// 返回文本字符串   
   }   
   
   /**
    * 去除html标签,但不取出<em>标签
    * @param html
    * @return
    */
   public static String trimHtmlTagWithOutEmTag(String html){
	 //处理高亮标签
		String emPre = "<em style='color:red'>";
		String emPos = "</em>";
		String tempFlag = UuidUtil.newGUID();
		//将高亮标签<em>替换为一个uuid
		html = html.replaceAll(emPre, tempFlag+"pre");
		html = html.replaceAll(emPos, tempFlag+"pos");
		//去除html标签
		html = HTMLUtil.Html2Text(html);   
		//将uuid替换为<em>标签
		html = html.replaceAll(tempFlag+"pre", emPre);
		html = html.replaceAll(tempFlag+"pos", emPos);
		return html;
   }
   
  

}
